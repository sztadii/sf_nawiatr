function FullWindow() {
  var height;
  var width;
  var objects;


  $(document).ready(function () {
    objects = $('.fullWindow');

    changeHeight();
  });

  $(window).bind('resize orientationchange', function () {
    if(width == $(window).width()) {
      return true;
    }

    changeHeight();
  });

  function changeHeight() {
    height = $(window).height();
    width = $(window).width();
    objects.css({'height': height});
  }
}

var fullWindow = new FullWindow();